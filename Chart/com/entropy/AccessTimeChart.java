package com.entropy;

import javax.swing.JFrame;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public class AccessTimeChart extends JFrame {
	
	private static final long serialVersionUID = 1L;
	
	ChartPanel myChart;
	JFreeChart chart;
	
    public AccessTimeChart(XYSeries series, String name) {
    	XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(series);
        //Tworzymy wykres XY
        chart = ChartFactory.createXYLineChart(
            null,//Tytu�
            "Step", // x-axis Opis
            "Acces time [ms]", // y-axis Opis
            dataset, // Dane
            PlotOrientation.VERTICAL, // Orjentacja wykresu /HORIZONTAL
            false, // pozka� legende
            true, // podpowiedzi tooltips
            false
        );
        
        myChart = new ChartPanel(chart);
        
        this.getContentPane().add(myChart);
    }
    
    public void refreshBoard() {
    	myChart.repaint();
    }
    
    public void test(String string) {
    	TextTitle  title = new TextTitle(string);
        chart.addSubtitle(title);
    }
}
