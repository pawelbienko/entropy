
package com.entropy;

import java.awt.*;
import java.util.ArrayList;

import javax.swing.*;

public class HostUIFrame extends JFrame
{
	private static final long serialVersionUID = 1L;
	
	BorderLayout borderLayout1 = new BorderLayout();
    JPanel pnl_main;
    private ArrayList<Thing> punkt;
    JLabel label1;
    JLabel label2;

    public HostUIFrame( ArrayList<Thing> point ) {
    	punkt = point;
    	
    	try {
            jbInit();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private void jbInit() throws Exception {
  
        pnl_main = new WorldPane(punkt); 

        label2 = new JLabel();
        label2.setFont(new Font("Dialog", Font.BOLD, 12));
        pnl_main.add(label2);
        
        this.add(pnl_main);
    }

    public void refreshBoard() {
    	pnl_main.repaint();
    }
    
    public void test(String string) {
    	 label2.setText(string);
    	 
    }
}


