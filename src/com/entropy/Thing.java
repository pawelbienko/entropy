package com.entropy;

import java.awt.Color;
import java.awt.Point;

public class Thing extends Point{

	Integer uid;
	Integer startedUid;
	Color color;
	Color startedColor;
	boolean empty = false;
	boolean startedEmpty;
	public int startedX;
	public int startedY;
	public String type;
	
	public Thing(int x, int y, Color color, boolean emtpy) {
		super(x, y);
		startedX = x;
		startedY = y;
				
		if(color == Color.GRAY ) {
			type = "gray";
		} else if( color == Color.BLUE) {
			type = "blue";
		} else if(color == Color.GREEN ){
			type = "green";
		} else if(color == Color.YELLOW) {
			type = "yellow";
		} else if( color == Color.RED ) {
			type = "red";
		}
		
		this.color = color;
		startedColor = color;
		empty = emtpy;
		startedEmpty = empty;
	}
	
	public void setUid(Integer uid) {
		this.uid = uid;
	}
	
	public Integer getUid() {
		return this.uid;
	}

	public void setStartedUid(Integer uid) {
		this.startedUid = uid;
	}
	
	public Integer getStartedUid() {
		return this.startedUid;
	}
	
	public Color getColor() {
		return color;
	}
	
	public void setColor(Color color) {
		this.color = color;
	}
	
	public void setEmptyFalse(){
		empty = false;
	}
	
	public void setEmptyTrue(){
		color = Color.GRAY;
		empty = true;
		uid = 0;
	}
	
	public void setType(String type) {
		type = type;
	}
	
	public String getType() {
		return type;
	}
	
	public boolean isEmpty(){
		return empty;
	}
	
	public boolean isNotCorrectlySet() {
		if(!startedUid.equals(uid) && type != "gray") {
			return true;
		}
		return false;
	}
	
	public int[] getCoordinans () {
		int[] dimension = {(int)this.getX(), (int)this.getY()};
		return dimension;
	}
	
	public int[] getStartedCoordinans () {
		int[] dimension = {(int)this.startedX, (int)this.startedY};
		return dimension;
	}
	
	public void setStartedCoordinans (int[] coor) {
		this.startedX = coor[0];
		this.startedY = coor[1];
	}
	
	public void setCoordinans (int[] coor) {
		this.x = coor[0];
		this.y = coor[1];
	}
}
