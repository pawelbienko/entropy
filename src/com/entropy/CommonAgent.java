package com.entropy;

import java.io.IOException;
import java.util.Random;

import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;

public class CommonAgent extends Agent {
	public static final int REQUEST = 1001;
	public static final int RESPONSE = 1002;
	public static final int WAIT = 1003;
	public static final int NOT_FOUND = 1004;
	public static final int RETURN = 1005;
	public static final int SEARCH = 1006;
	public static final int SEARCH_RESPONSE = 1007;
	public static final int FOUND = 1008;
	public static final int RECEIVE_FAIL = 1009;
	public static final int BLOCK_RESPONSE = 1010;
	public static final int OK = 1011;
	public static final int REQUEST_SAME = 1012;
	public static final int TIDY_UP = 1013;
	public static final int TIDIED_UP = 1014;
	
	public static final String HAWK = "hawk";
	public static final String DOVE = "dove";

	
	
	public Thing getSentObject(ACLMessage msg) {
    	Thing thing = null;
    	
    	try{
			 thing = (Thing) msg.getContentObject();
		}catch(UnreadableException e){}
    	
    	return thing;
    }
    
    public ACLMessage setSentObject(ACLMessage reply, Thing thing) {
    	try {
			reply.setContentObject(thing);			
		} catch (IOException e) {}
	
		return reply;
    }
    
    public void log(String text) {
    	System.out.format("Agent o nazwie %s zalogowa� wiadomo�� %s \n", getAID().getName(), text);
    }
    
    public void logAction(String name, String text) {
    	System.out.format("\n Agent o nazwie %s zalogowa� akcje o nazwie %s i tre�ci %s \n", getAID().getName(), name, text);
    }
    
    public void logThing(Thing thing, String text) {
    	System.out.format(
    		"Agent o nazwie %s zalogowa� obiekt o uid %d, startedUid %d, empty %s, kolorze %s, startowym kolorze %s,  i o koordynatach %d:%d oraz stratowe %d:%d i wiadomo�� o tre�ci %s \n",
    		getAID().getName(),
    		thing.getUid(),
    		thing.getStartedUid(),
    		thing.empty,
    		thing.getColor(),
    		thing.startedColor,
    		thing.getCoordinans()[0], 
    		thing.getCoordinans()[1],
    		thing.getStartedCoordinans()[0],
    		thing.getStartedCoordinans()[1],
    		text
    		);
    }
    
    public void logMessage(ACLMessage msg) {
    	System.out.format(
    		"Agent o nazwie %s otrzyma� wiadomo�� od %s o typie %s UID %d i koordynatach obiektu %d:%d oraz stratowe %d:%d \n", 
    		getAID().getName(),
    		msg.getSender().getName(),
    		msg.getPerformative(),
    		getSentObject(msg).getUid(),
    		getSentObject(msg).getCoordinans()[0], 
    		getSentObject(msg).getCoordinans()[1],
    		getSentObject(msg).getStartedCoordinans()[0],
    		getSentObject(msg).getStartedCoordinans()[1]						
    	);
    }
    
    public static int getRandomNumberInRange(int min, int max) {

		if (min >= max) {
			throw new IllegalArgumentException("max must be greater than min");
		}

		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}
}
