package com.entropy;

import java.awt.Dimension;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import java.util.*;
import java.util.concurrent.TimeUnit;
import org.jfree.data.xy.XYSeries;

public class WorldAgent extends CommonAgent
{
	private static final long serialVersionUID = 1L;
    
	protected HostUIFrame m_frame = null;
	protected EntropyChart entropy_frame;
	
	public ACLMessage messageUser1;
    public ACLMessage messageUser2;
    public Common common;
    public XYSeries series = new XYSeries("Entropy");
    
	public boolean user1 = false;
    public boolean user2 = false;
    public boolean returnChiter = false;
    public boolean returnGood = false;
    public boolean synchro = false;

    public int count = 0;
	public double maxEntropy = 0;
	public double sumEntropy = 0;
    public long startTime;

    public WorldAgent() {
    	startTime = System.currentTimeMillis();
    }
    
    public void coordinator(ACLMessage msg, boolean synchro) {
    	if (msg != null) {
 			// logMessage(msg);
 			
 			response(gameTheory(msg));
 		 }
    }
    
    public ACLMessage dispatcher(ACLMessage msg) {
    	ACLMessage reply = null;
    	
    	switch(msg.getPerformative()) {
			case REQUEST:
				reply = sendThing(msg);
				break;
			case REQUEST_SAME:
				reply = sendThing(msg);
				break;
			case RETURN:
				reply = receiveThing(msg);
		    	break;
			case SEARCH:
				reply = searchThing(msg);
				break;
    		case WAIT: 
    			reply = sendWaitThing(msg);
    			break;
    		case TIDIED_UP: 
    			reply = sendTidyUpThing(msg);
    			break;
			default:
				break;
		}
    	
    	return reply;
    }
    
    public void response(ArrayList<ACLMessage> replies) {
    	for(ACLMessage reply : replies) {
    		// logThing(getSentObject(reply), "Obiekty wysylany w odpowiedzi");
    		send(reply);
    	}
    	    	
		refreshBoard();
    }
	
    @Override
    protected void setup() {
    	Object[] args = getArguments();

        common = new Common(new Dimension(Integer.parseInt(args[0].toString()), Integer.parseInt(args[0].toString())));
    	setupUI();
    	
    	series.add(0, calculateEntropy());
        
    	addBehaviour(new CyclicBehaviour(this) {
			private static final long serialVersionUID = 1L;

			@Override
    		 public void action() {
    			 ACLMessage msg = receive();
    			 
    			 if((System.currentTimeMillis() - startTime) <= (5 * 1000)) {
    			 	coordinator(msg, true);
    			 } else {
					System.exit(0);
    			 	doDelete();
				 }
    		 }
    	 });
    }
    
    protected ACLMessage searchThing(ACLMessage msg) {   	
    	// logAction("searchThing", "Agent world szuka obiektu");
    	Thing sentThing = getSentObject(msg);
    	// logThing(sentThing, "agent odebra� taki obiekt do szukania");
        
    	ACLMessage reply = msg.createReply();
    	Thing foundThing = common.findThing(sentThing);
    	// logThing(foundThing, "agent odnalaz� taki obiekt o startowych koordynatach");
            	
    	if(foundThing.getUid() != null && foundThing.getUid().intValue() == sentThing.getUid().intValue()) {
    		// logAction("searchThing", "FOUND");
    		reply.setPerformative(FOUND);
    		reply = setSentObject(reply, foundThing);
    	} else {
    		reply.setPerformative(NOT_FOUND);
    		reply = setSentObject(reply, foundThing);
    	}
    	
    	return reply;		
    }
   
    protected ACLMessage sendWaitThing(ACLMessage msg) {
    	// logAction("sendWaitThing", "rozpocz�� wysy�anie ��danego obiektu");
    	Thing sentThing = getSentObject(msg);
    	// logThing(sentThing, "obiekt ��dany");
  	  
    	ACLMessage reply = msg.createReply();
    	Thing foundThing = common.findThing(sentThing);
    	// logThing(foundThing, "obiekt odszukany");
    	
		// logAction("sendThing", "WAIT");
		reply.setPerformative(WAIT);
		reply = setSentObject(reply, sentThing);
    	
		return reply;
    }
    
    protected ACLMessage sendTidyUpThing(ACLMessage msg) {
    	// logAction("sendTidyUpThing", "rozpocz�� wysy�anie ��danego obiektu");
    	Thing sentThing = getSentObject(msg);
    	// logThing(sentThing, "obiekt ��dany");
  	  
    	ACLMessage reply = msg.createReply();
    	Thing foundThing = common.findThing(sentThing);
    	// logThing(foundThing, "obiekt odszukany");
    	
    	Thing foundThingStarted = common.findStartedThingByUid(sentThing.uid);
    	// logThing(foundThingStarted, "startowy obiekt odszukany");
    	    	
    	foundThing.setColor(sentThing.getColor());

    	foundThing.setEmptyFalse();
    	foundThing.setUid(sentThing.getStartedUid());
    	foundThing.setColor(sentThing.startedColor);

    	// logThing(foundThing, "status obiekt kt�ry zosta� zapisany w macierzy");
	    	
		// logAction("receiveThing", "TIED_UP");
		reply.setPerformative(TIDIED_UP);
		reply = setSentObject(reply, foundThing);

		double entropy = calculateEntropy();
		
		if (entropy > maxEntropy) {
			maxEntropy = entropy;
		}
		
		sumEntropy += entropy;		
		count++;
		
		// // logAction("Wynik entropii", count + " : " + entropy + "�rednia: " + sumEntropy/count + "entropy: " + maxEntropy );
    	series.add(count, entropy);

    	entropy_frame.refreshBoard();	
				
		return reply;
    }
    
    protected ACLMessage sendThing(ACLMessage msg) {
    	// logAction("sendThing", "rozpocz�� wysy�anie ��danego obiektu");
    	Thing sentThing = getSentObject(msg);
    	// logThing(sentThing, "obiekt ��dany");
  	  
    	ACLMessage reply = msg.createReply();
    	Thing foundThing = common.findThing(sentThing);
    	// logThing(foundThing, "obiekt odszukany");
    	
    	if(foundThing.getUid() != null && foundThing.getUid().equals(sentThing.getUid())) {
    		// logAction("sendThing", "RESPONSE");
    		reply.setPerformative(RESPONSE);
    		reply = setSentObject(reply, foundThing);
    		foundThing.setEmptyTrue();
    	} else if(foundThing.getUid() == null || !foundThing.getUid().equals(sentThing.getUid()) ){
    		// logAction("sendThing", "NOT_FOUND");
    		reply.setPerformative(NOT_FOUND);
    		reply = setSentObject(reply, sentThing);
    	} else {
    		// logAction("sendThing", "nieznana opcja");
    	}
    	
    	return reply;
    }
    
    protected ACLMessage receiveThing(ACLMessage msg) {
    	// logAction("receiveThing", "otrzyma�em obiekt z powrotem");
    	Thing sentThing = getSentObject(msg);
    	// logThing(sentThing, "obiekt zwracany");
    	
    	ACLMessage reply = msg.createReply();
    	
    	Thing foundThing = common.findThing(sentThing);
    	// logThing(foundThing, "obiekt odszukany");
    	
    	foundThing.setColor(sentThing.getColor());

    	foundThing.setEmptyFalse();
    	foundThing.setUid(sentThing.getUid());

    	// logThing(common.findThing(foundThing), "status obiekt kt�ry zosta� zapisany w macierzy");
    	     	
		// // logAction("receiveThing", "OK");
		reply.setPerformative(OK);
		reply = setSentObject(reply, sentThing);

		double entropy = calculateEntropy();
		
		if (entropy > maxEntropy) {
			maxEntropy = entropy;
		}
		
		sumEntropy += entropy;
		
		count++;
		if(synchro) {
			if(entropy == 0) {
				// logAction("Wynik entropii", count + " : " + entropy + "�rednia: " + sumEntropy/count + "entropy: " + maxEntropy );
		    	series.add(count, entropy);
			}
		} else {
			// logAction("Wynik entropii", count + " : " + entropy + "�rednia: " + sumEntropy/count + "entropy: " + maxEntropy );
	    	series.add(count, entropy);
		}
	
    	entropy_frame.refreshBoard();	
				
		return reply;
    }
    
    public ArrayList<ACLMessage> gameTheory(ACLMessage msg) { 
    	ArrayList<ACLMessage> result = new ArrayList<ACLMessage>();
    	
      	if(msg.getSender().getName().contains("chiter")) {
      		if(msg.getPerformative() == RETURN) {
      			// logAction("gameTheory", "Agent chiter zwraca obiekt");
				result.add(dispatcher(msg));
				
				return result;
			}
      		// logAction("gameTheory", "Agent chiter ��da� obiektu");
      		
			messageUser1 = msg;
			user1 = true;
		}
      	
		if(msg.getSender().getName().contains("good")) {
			if(msg.getPerformative() == RETURN) {
				//returnGood = true;
				// logAction("gameTheory", "Agent good zwraca obiektu");
				result.add(dispatcher(msg));
				
				return result;
			}
			// logAction("gameTheory", "Agent good ��da� obiektu");
						
			messageUser2 = msg;
			user2 = true;
		}

		if(user1 == true && user2 == true) {	   	
			user1 = false;
			user2 = false;
			Thing thing1 = getSentObject(messageUser1);
			Thing thing2 = getSentObject(messageUser2);
			
			if(messageUser1.getPerformative() == REQUEST_SAME) {
				result.add(dispatcher(messageUser2));
				result.add(dispatcher(messageUser1));
				
				return result;
			} else if(messageUser2.getPerformative() == REQUEST_SAME) {
				result.add(dispatcher(messageUser1));
				result.add(dispatcher(messageUser2));
				
				return result;
			}
			
			if ( thing1.getCoordinans()[0] == thing2.getCoordinans()[0] && thing1.getCoordinans()[1] == thing2.getCoordinans()[1]) { //|| thing1.getUid().intValue() == thing2.getUid().intValue()) {
				if(messageUser1.getSender().getName().contains("chiter")) {
					// logAction("gameTheory", "agencji ��daj� tych samych obiekt�w");
					
			      	String tacticUser1 = messageUser1.getUserDefinedParameter("tactic");
			      	String tacticUser2 = messageUser2.getUserDefinedParameter("tactic");
			      	
			      	// logAction("gameTheory", "Taktyka 1:" + tacticUser1 + " : " + HAWK + "2: " + tacticUser2);
								      	
			      	if (tacticUser1.equals(DOVE) && tacticUser2.equals(HAWK)) {		      		
			      		result.add(dispatcher(messageUser2));
						messageUser1.setPerformative(WAIT);
						result.add(dispatcher(messageUser1));
			      	} else if (tacticUser1.equals(HAWK) && tacticUser2.equals(DOVE)) {
			      		result.add(dispatcher(messageUser1));
						messageUser2.setPerformative(WAIT);
						result.add(dispatcher(messageUser2));
			      	} else if (tacticUser1.equals(tacticUser2) && tacticUser2.equals(HAWK)) {
			      		fight(result);
			      		// kiedy jastrz�bie walcz� trac� 500ms
			      		try {
							TimeUnit.MILLISECONDS.sleep(500);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
			      	} else if (tacticUser1.equals(tacticUser2) && tacticUser2.equals(DOVE)) {
			      		// kiedy go��bie stosz� pi�rka obaj agenci trac� 2 sekundy
			      		try {
							TimeUnit.SECONDS.sleep(2);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
			      		fight(result);
			      	} else if (tacticUser1.equals(tacticUser2) && tacticUser2.equals("none")) {
			      		// kiedy agencji nie maj� �adnej taktyki to w konfliktach dajemy losowo dost�p.
			      		fight(result);
			      	}	
				}
			} else {
				// logAction("gameTheory", "agencji ��daj� r�nych obiekt�w");
				result.add(dispatcher(messageUser1));
				result.add(dispatcher(messageUser2));
			}
			messageUser1 = null;
			messageUser2 = null;
		}
		
		return result;
    }
    
    public void fight(ArrayList<ACLMessage> result) {
    	// prawdopodbienstwo wygrania przez user1
  		if(common.wonProbability(0.5)) {
			// logAction("wonProbability", "agent1 wygrywa");
			result.add(dispatcher(messageUser1));
			messageUser2.setPerformative(WAIT);
			result.add(dispatcher(messageUser2));
		} else {
			// logAction("wonProbability", "agent2 wygrywa");
			result.add(dispatcher(messageUser2));
			messageUser1.setPerformative(WAIT);
			result.add(dispatcher(messageUser1));
		}
    }

    public double calculateEntropy() {
    	double entropy = 0.0;
    	
    	double uncorrectlySetted = 0.0;
    	double allElement = 0.0;
    	double k;
    	
    	Map<String, Integer> freqType = new HashMap<String, Integer>();
    	
    	for (Thing thing: common.thingsList) {
        	if(thing.isNotCorrectlySet()) {
        		// logAction("notCorrectlyset", thing.getType() + "");
        		if( thing.getType() == "blue") {
        			Integer count = freqType.get(thing.getType());
        			if (count == null) {
        				freqType.put(thing.getType(), 1);
        			}
        			else {
        				freqType.put(thing.getType(), count + 1);
        			}
        		} else if( thing.getType() == "green" ){
        			Integer count = freqType.get(thing.getType());
        			if (count == null) {
        				freqType.put(thing.getType(), 1);
        			}
        			else {
        				freqType.put(thing.getType(), count + 1);
        			}
        		} else if( thing.getType() == "yellow" ) {
        			Integer count = freqType.get(thing.getType());
        			if (count == null) {
        				freqType.put(thing.getType(), 1);
        			}
        			else {
        				freqType.put(thing.getType(), count + 1);
        			}
        		} else if( thing.getType() == "red" ) {
        			Integer count = freqType.get(thing.getType());
        			// logAction("count", count + "");
        			if (count == null) {
        				freqType.put(thing.getType(), 1);
        			}
        			else {
        				freqType.put(thing.getType(), count + 1);
        			}
        		}
        		++ uncorrectlySetted;
        	}
        	++ allElement;
         }
    	
    	// logAction("calculateEntropy", "uncorrect: " + uncorrectlySetted + "all: " + allElement);

    	// liczba typ�w obiekt�w
    	double m_e = 24;
    	// liczba element�w kt�re s� na swoich miejscach
    	double M = 0;
    	double m_1 = freqType.get("blue") == null ? 0 : freqType.get("blue");
    	double m_2 = freqType.get("green") == null ? 0 : freqType.get("green");
    	double m_3 = freqType.get("yellow") == null ? 0 : freqType.get("yellow");
    	double m_4 = freqType.get("red") == null ? 0 : freqType.get("red");
    	
    	// logAction("silnia M", M + "");
    	
    	// logAction("silnia", MathHelper.silnia(m_e + uncorrectlySetted) + "");
    	// logAction("silnia", MathHelper.silnia(m_1) + "");
    	// logAction("silnia", MathHelper.silnia(m_2) + "");
    	// logAction("silnia", MathHelper.silnia(m_3) + "");
    	// logAction("silnia", MathHelper.silnia(m_4) + "");
    	    	
    	k = MathHelper.silnia(m_e + uncorrectlySetted) / ( MathHelper.silnia(m_e) * MathHelper.silnia(m_1) * MathHelper.silnia(m_2) * MathHelper.silnia(m_3) * MathHelper.silnia(m_4) ) ; 

    	// logAction("calculateEntropy", "sposoby: " + k);

	    entropy = - ((k - 1) * ((1 / k) * Math.log(1 / k))); 

    	// logAction("calculateEntropy", "entropy: " + entropy);
    	if (Double.isNaN(entropy)) {
    		entropy = 0;
    	}

		System.out.format("\n dispatcher entropy: %s", entropy);
		    	
    	return entropy;
    }
    
    private void refreshBoard() {    	
    	Map<String, Integer> freqType = new HashMap<String, Integer>();
    	freqType.put("blue", 0);
		freqType.put("green", 0);
		freqType.put("yellow", 0);
		freqType.put("red", 0);
		
    	for (Thing thing: common.thingsList) {
        	if(thing.isNotCorrectlySet()) {

        		if( thing.getType() == "blue") {
        			Integer count = freqType.get(thing.getType());
        			if (count == null) {
        				freqType.put(thing.getType(), 1);
        			}
        			else {
        				freqType.put(thing.getType(), count + 1);
        			}
        		} else if( thing.getType() == "green" ){
        			Integer count = freqType.get(thing.getType());
        			if (count == null) {
        				freqType.put(thing.getType(), 1);
        			}
        			else {
        				freqType.put(thing.getType(), count + 1);
        			}
        		} else if( thing.getType() == "yellow" ) {
        			Integer count = freqType.get(thing.getType());
        			if (count == null) {
        				freqType.put(thing.getType(), 1);
        			}
        			else {
        				freqType.put(thing.getType(), count + 1);
        			}
        		} else if( thing.getType() == "red" ) {
        			Integer count = freqType.get(thing.getType());
        			if (count == null) {
        				freqType.put(thing.getType(), 1);
        			}
        			else {
        				freqType.put(thing.getType(), count + 1);
        			}
        		}
        	}
         }
    	m_frame.setTitle("Observation time : " + Long.toString((System.currentTimeMillis() - startTime) / (1000)) + "sek. Incorrectly set:{red " + freqType.get("red") + ", green: " + freqType.get("green") + ", blue: " + freqType.get("blue") + ", yellow: " + freqType.get("yellow")  + "}");
        ((HostUIFrame) m_frame).refreshBoard();         
    }
    
    private void setupUI() {
        m_frame = new HostUIFrame( common.thingsList );

        m_frame.setSize( 600, 600 );
        m_frame.setLocation( 400, 300 );
        m_frame.setVisible( true );
        m_frame.validate();
        
        entropy_frame = new EntropyChart(series);
        entropy_frame.setTitle("Entropy chart");
        entropy_frame.setVisible(true);
        entropy_frame.setSize(510,400);
    }

    public void takeDown(){
    	log("=================================== end ======================================");
    	log("\n world kill");
	}
}


