package com.entropy;

import java.awt.Color;
import java.awt.Dimension;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;

public class Common {
    public ArrayList<Thing> thingsList = new ArrayList<Thing>();
    public ArrayList<Thing> thingsListStarted = new ArrayList<Thing>();
	    
    public Common(Dimension gameBoardSize) {
    	thingsList = Common.randomlyFillBoard(gameBoardSize, thingsList);
    	thingsListStarted = thingsList;
    }
    
    public static ArrayList<Thing> randomlyFillBoard(Dimension gameBoardSize, ArrayList<Thing> thingsList) {
    	ArrayList<Color> colorMap = new ArrayList<Color>(0);
    	
    	for (int i=0; i < (gameBoardSize.width * gameBoardSize.height); i++) {
    		if( i < gameBoardSize.width) {
    			colorMap.add(Color.GRAY);
    		} else if(i >= gameBoardSize.width && i < (3.5 * gameBoardSize.width) ) {
    			colorMap.add(Color.BLUE);
    		} else if(i >= (3.5 * gameBoardSize.width) && i < (6 * gameBoardSize.width) ) {
    			colorMap.add(Color.GREEN);
    		} else if(i >= (6 * gameBoardSize.width) && i < (8.5 * gameBoardSize.width) ) {
    			colorMap.add(Color.YELLOW);
    		} else if(i >= (8.5 * gameBoardSize.width) && i < (11 * gameBoardSize.width) ) {
    			colorMap.add(Color.RED);
    		} else if (i >=  (11 * gameBoardSize.width)) {
    			colorMap.add(Color.GRAY);
    		}
    	}

    	Integer uid = 0;
        for (int i=0; i < gameBoardSize.width; i++) {	
            for (int j=0; j < gameBoardSize.height; j++) {
            	if(colorMap.get(0) == Color.GRAY) {
            		Thing thing = new Thing(i, j, colorMap.get(0), true);
            		thing.setStartedUid(0);
            		thing.setUid(0);
            		//thing.setEmptyTrue();
            		thingsList.add(thing);
	                colorMap.remove(0);
            	} else {
            		Thing thing = new Thing(i, j, colorMap.get(0), false);
            		thing.setStartedUid(uid);
            		thing.setUid(uid);
            		thingsList.add(thing);
	                colorMap.remove(0);
            	}
            	uid ++;
            }
        }
        
        return thingsList;
    }
    
    public Thing findThing(Thing sentThing) {
        for(Thing thing : thingsList) {
            if(thing.getLocation().equals(sentThing.getLocation())) {
                return thing;
            } 
        }
        return null;
    }
    
    public Thing findThingByUid(Integer uid) {
        for(Thing thing : thingsList) {
            if(thing.uid.equals(uid)) {
                return thing;
            } 
        }
        return null;
    }
    
    public Thing findStartedThingByUid(Integer uid) {
        for(Thing thing : thingsListStarted) {
            if(thing.uid.equals(uid)) {
                return thing;
            } 
        }
        return null;
    }
    
    public Thing findThingByStartedCoordinans(Thing sentThing) {
        for(Thing thing : thingsList) {
            if(
            	thing.getCoordinans()[0] == sentThing.getStartedCoordinans()[0]
            	&& thing.getCoordinans()[1] == sentThing.getStartedCoordinans()[1]
            ) {
                return thing;
            } 
        }
        return null;
    }
    
    public boolean chitingProbability(double probability) {
    	Random generator = new Random();
    	double number = generator.nextFloat();

    	if(number <= probability) {
    		return true;
    	}
    	
    	return false;
    }
    
    public boolean wonProbability(double probability) {
    	return chitingProbability(probability);
    }
}
