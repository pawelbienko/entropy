package com.entropy;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;
import org.jfree.data.xy.XYSeries;
import java.awt.Dimension;
import java.lang.String;
import jade.core.AID;
import jade.lang.acl.ACLMessage;
import jade.util.leap.Properties;
import jade.core.behaviours.TickerBehaviour;

public abstract class GuestAgent extends CommonAgent
{
	private static final long serialVersionUID = 1L;
	
	AID world;
	int tickTime;
	int matrixSize;
	double chittingPropability;
	
	int idOfSearch = 0;
	
	int countTidyUp = 50;
	boolean tidyUp = false;
	long startTime;
	long workingTime;
	long elapsedTimePrevious = 0;	
	String tactic;
	int count = 0;
	int effort = 0;
	double effortSum = 0;
	int maxEffort = 0;
	long medTime;
	double maxElapsedTime = 0;
    Common common;
	AccessTimeChart frame1;
	EffortChart frame2;
    XYSeries accessTimeSeries = new XYSeries("Access time");
    XYSeries effortSeries = new XYSeries("Effort");
    boolean block = false;
    String name;
               
    public GuestAgent(String name) {
		workingTime = System.currentTimeMillis();
    	this.name = name;
		frame1 = new AccessTimeChart(accessTimeSeries, name);
        frame1.setVisible(true);
        frame1.setSize(510,400);
        
        frame2 = new EffortChart(effortSeries, name);
        frame2.setVisible(true);
        frame2.setSize(510,400);       
	}
        
    public ArrayList<Thing> getEmpty(){
    	ArrayList<Thing> emptyList = new ArrayList<Thing>();
    	
    	for (int i=0; i < (matrixSize * matrixSize); i++) {
    		if(common.thingsList.get(i).getUid().intValue() == 0) {
    			emptyList.add(common.thingsList.get(i));
    		}
    	}
    	
    	return emptyList;
    }
    
    public ArrayList<Thing> getNotEmpty(){
    	ArrayList<Thing> notEmptyList = new ArrayList<Thing>();
    	
    	for (int i=0; i < (matrixSize * matrixSize); i++) {
    		if(common.thingsList.get(i).getUid().intValue() != 0) {
    			notEmptyList.add(common.thingsList.get(i));
    		}
    	}
    	
    	return notEmptyList;
    }
    
    public ArrayList<Thing> getMoveThing(){
    	ArrayList<Thing> moveList = new ArrayList<Thing>();
    	
    	for (int i=0; i < ( matrixSize * matrixSize); i++) {
    		if(!common.thingsList.get(i).getUid().equals(common.thingsList.get(i).getStartedUid())) {
    			moveList.add(common.thingsList.get(i));
    		}
    	}
    	
    	return moveList;
    }
    
    public void coordinator(ACLMessage msg) {
		response(dispatcher(msg));
    }
    
    public void response(ACLMessage reply) {
    	if(reply != null) {
    		// // logAction("sendThing", "");
          	Properties prop = new Properties();
          	prop.setProperty("tactic", tactic);
          	reply.setAllUserDefinedParameters(prop);
          	
    		send(reply);
    	}
    }
    
    public ACLMessage dispatcher(ACLMessage msg) {
    	ACLMessage reply = null;
    	
    	if ( msg != null) {
    		// logMessage(msg);
    		effort++;
    		effortSum++;
    		switch(msg.getPerformative()) {
    		case RESPONSE:
    			// logAction(Boolean.toString(block), "RESPONSE: status blokady");
    			reply = returnThing(msg);
    			break;
    		case NOT_FOUND:
    			// logAction(Boolean.toString(block), "NOT_FOUND: status blokady");
    			reply = searchThing(msg);
    			break;
    		case FOUND:
    			// logAction(Boolean.toString(block), "FOUND: status blokady");
    			reply = returnFoundThing(msg);
    			break;
    		case TIDIED_UP:
    			// logAction(Boolean.toString(block), "TIDIED_UP: status blokady");
    			reply = returnFoundThing(msg);
    			break;
    		case WAIT:
    			// logAction(Boolean.toString(block), "WAIT: status blokady");
    			reply = getSameThing(msg);
    			break;
    		case OK:
    			// logAction(Boolean.toString(block), "OK: status blokady");
    			refreshChart();
    			effort = 0;
    			
    			startTime = System.currentTimeMillis();
    			reply = getRandomThing();
    			break;
    		}
		} else {
	    	if(!block) {
	    		startTime = System.currentTimeMillis();
		    	// logAction(Boolean.toString(block), "getRandomThing status blokady");
	    		reply = getRandomThing();
	    	}        	    	
		}
    	
    	return reply;
    }
    
    protected void setup() {
    	world = getAID("world");
    	
    	Object[] args = getArguments();
        tickTime = Integer.parseInt(args[0].toString());
        matrixSize = Integer.parseInt(args[1].toString());
        chittingPropability = Integer.parseInt(args[2].toString()) / 10.0;
        tactic = args[3].toString();
        
        frame1.setTitle( name + " tactics: " + tactic + " cheating probability: " + chittingPropability);
        frame2.setTitle( name + " tactics: " + tactic + " cheating probability: " + chittingPropability);
    	
        common = new Common(new Dimension(Integer.parseInt(args[1].toString()), Integer.parseInt(args[1].toString())));
        
        accessTimeSeries.add(0, 0);
        effortSeries.add(0, 0);
        
        addBehaviour(new TickerBehaviour(this, tickTime) {
			private static final long serialVersionUID = 1L;
			@Override
        	public void onTick() {
        		ACLMessage msg = receive();

				if((System.currentTimeMillis() - workingTime) <= (5 * 1000)) {
					coordinator(msg);
				} else {
					System.exit(0);
					doDelete();
				}
        	}
        });
    }
     
    public Thing getRandomNotEmptyThing() {
    	ArrayList<Thing> notEmptyList = getNotEmpty();
    	int randomNum = ThreadLocalRandom.current().nextInt(0, notEmptyList.size()-1);
        Thing thing = notEmptyList.get(randomNum);
        // logAction("getRandomNotEmptyThing", "rozmiar tablicy niepustych: " + notEmptyList.size());
		return thing;
    }
    
    protected ACLMessage getRandomThing() {
    	block = true;
    	// logAction("getRandomThing", "REQUEST");
    	
    	// logAction("getRandomThing", "rozmiar tablicy prze�o�onych: " + getMoveThing().size());
    	
    	if(count > countTidyUp && tidyUp && (getMoveThing().size() > 0)) {
    		ACLMessage request = new ACLMessage(TIDIED_UP);
    		request.addReceiver(world);
        	
    		Thing moveThing = getMoveThing().get(0);
    		
			// logAction("chitingProbability", "agent sprz�ta" + chittingPropability);
		
	    	// logThing(moveThing, "agent oddaje taki obiekt");
	        
	    	request = setSentObject(request, moveThing);  

			return request;	
		}
    	
    	Thing notEmptyThing = getRandomNotEmptyThing();
    	// logThing(notEmptyThing, "taki obiekt agent wybra� do za��dania");

    	ACLMessage request = new ACLMessage(REQUEST);
		request.addReceiver(world);
    	
		request = setSentObject(request, notEmptyThing);
		notEmptyThing.setEmptyTrue();
		
		return request;
    }
    
    protected ACLMessage getSameThing(ACLMessage msg) {
    	// logAction("getSameThing", "REQUEST_SAME");
    	Thing sentThing = getSentObject(msg);
    	// logThing(sentThing, "Obiekt powinien mie� takie same koordynaty");
    	
    	ACLMessage reply = msg.createReply();
    	
    	reply.setPerformative(REQUEST_SAME);
		reply = setSentObject(reply, sentThing);

		return reply;
    }
    
    protected ACLMessage searchThing(ACLMessage msg) {
    	// logAction("searchThing", "SEARCH");
    	Thing sentThing = getSentObject(msg);
    	// logThing(sentThing, "agent odebra� obiekt");
    	
    	// taki obiekt znaleziono pod koordynatami do zapisania w pami�ci
    	Thing thingToSave = common.findThing(sentThing);
    	
    	thingToSave.setColor(sentThing.getColor());	    	
    	thingToSave.setEmptyFalse();
    	thingToSave.setUid(sentThing.getUid());
		// logThing(thingToSave, "agent zapisa� w swojej pami�ci taki obiekt");
    	
    	if(idOfSearch == 143) {
    		// Zaczynamy od nowa szuka� 
    		idOfSearch = 0;
    	}
    	
    	Thing searchThing = null;
    	searchThing = common.thingsList.get(idOfSearch++);
    	
    	searchThing.setUid(sentThing.getUid());
    	// logThing(searchThing, "agent szuka �adanego obiekt pod takimi koordyantami");
    	
    	ACLMessage reply = msg.createReply();
    	reply.setPerformative(SEARCH);
		reply = setSentObject(reply, searchThing);

		return reply;
    }
    
    protected ACLMessage returnFoundThing(ACLMessage msg) {
    	// logAction("returnThing", "RETURN");
    	idOfSearch = 0;
    	Thing thing = getSentObject(msg);
    	// logThing(thing, "agent przyj�� obiekt do oddania");
    	
		ACLMessage response = msg.createReply(); 
		
		Thing sendThing;
		sendThing = common.findThing(thing);
    	
		sendThing.setColor(thing.getColor());	    	
		sendThing.setEmptyFalse();
		sendThing.setUid(thing.getUid());
		// logThing(sendThing, "agent zapisa� w swojej pami�ci taki obiekt");
		
    	// logThing(thing, "agent oddaje taki obiekt");
        response = setSentObject(response, thing);  

		response.setPerformative(RETURN);
		
		return response;
    }
    
    public Thing getRandomEmptyThing() {
    	ArrayList<Thing> emptyList = getEmpty();

        Thing thing = emptyList.get(getRandomNumberInRange(0, emptyList.size() - 1));
        // logAction("getRandomEmptyThing", "rozmiar tablicy pustych: " + emptyList.size());
		return thing;
    }
    
    protected ACLMessage returnThing(ACLMessage msg) {
    	// logAction("returnThing", "RETURN");
    	idOfSearch = 0;
    	Thing thing = getSentObject(msg);
    	// logThing(thing, "agent przyj�� obiekt do oddania");
    	
		ACLMessage response = msg.createReply(); 
		
		Thing sendThing;
		
		if((count > countTidyUp && tidyUp)) {
			// logAction("chitingProbability", "agent nie oszukuje" + chittingPropability);
			sendThing = common.findThing(thing);
		} else {
			if(common.chitingProbability(chittingPropability)) {
				// logAction("chitingProbability", "agent oszukuje" + chittingPropability);
				sendThing = getRandomEmptyThing();
			} else {
				// logAction("chitingProbability", "agent nie oszukuje" + chittingPropability);
				sendThing = common.findThing(thing);
			}
		}
    	
		sendThing.setColor(thing.getColor());	    	
		sendThing.setEmptyFalse();
		sendThing.setUid(thing.getUid());
    	// logThing(sendThing, "agent oddaje taki obiekt");
        
		response = setSentObject(response, sendThing);  

		response.setPerformative(RETURN);
		
		return response;
    }
    
    protected void refreshChart() {
    	double elapsedTime = System.currentTimeMillis() - startTime;
    	double midEffort;
    	
    	if(elapsedTime > maxElapsedTime) {
			maxElapsedTime = elapsedTime;
		}

    	medTime += elapsedTime;
    	++count;
    	// logAction("wykres czasu: "," count: " + count + " elapsedTime: " + elapsedTime + " medTime: " + medTime /count + " max: " + maxElapsedTime);
    	
		accessTimeSeries.add(count, elapsedTime);
		
		if(effort > maxEffort) {
			maxEffort = effort;
		}
		
		midEffort = effortSum/count;
		
		System.out.format("\n" + name + " effort: %s " + name + " elapsedTime: %s", effort, elapsedTime);
		
		//if((effort) > 2) {
			//logAction("refreshChart", "count: " + count + "wysi�ek wi�kszy ni� dwa: " + (effort) + "maxEffort: " + maxEffort + "midEffort: " + midEffort + "effortSum: " + effortSum);
		//}
		effortSeries.add(count, (effort));
    }

	public void takeDown(){
		log(this.name + " kill");
	}
}

