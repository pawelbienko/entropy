package com.entropy;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Random;

import javax.swing.JPanel;

public class WorldPane extends JPanel {
	
    private ArrayList<Thing> pointsList;
    static final int BLOCK_SIZE = 40;
    Dimension gameBoardSize = new Dimension(12, 12);
    
	public WorldPane(ArrayList<Thing> pointsList) {
		this.pointsList = pointsList;
    }

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        try {
            for (Thing newPoint : pointsList) {
            	g.setColor(newPoint.getColor());
            	                
                g.fillRect(
                	BLOCK_SIZE + (BLOCK_SIZE * newPoint.x), 
                	BLOCK_SIZE + (BLOCK_SIZE * newPoint.y), 
                	BLOCK_SIZE, BLOCK_SIZE
                );
            }
        } catch (ConcurrentModificationException cme) {}
        
        // Setup grid
        g.setColor(Color.BLACK);
        for (int i=0; i <= gameBoardSize.width; i++) {
            g.drawLine(
            		((i * BLOCK_SIZE) + BLOCK_SIZE), 
            		BLOCK_SIZE,
            		(i * BLOCK_SIZE) + BLOCK_SIZE, BLOCK_SIZE + (BLOCK_SIZE * gameBoardSize.height)
            );
        }
        
        for (int i=0; i <= gameBoardSize.height; i++) {
            g.drawLine(
        		BLOCK_SIZE, 
        		((i * BLOCK_SIZE) + BLOCK_SIZE), 
        		BLOCK_SIZE * (gameBoardSize.width + 1), 
        		((i * BLOCK_SIZE) + BLOCK_SIZE)
        	);
        }
    }
    
//    public void randomlyFillBoard(int percent) {
//        for (int i=0; i < this.gameBoardSize.width; i++) {
//            for (int j=0; j < this.gameBoardSize.height; j++) {
//                if (Math.random()*100 < percent) {
//                    addPoint(i,j);
//                }
//            }
//        }
//    }
//    
//    public void addPoint(int x, int y) {
//        if (!host.point.contains(new Point(x,y))) {
//        	host.point.add(new Point(x,y));
//        } 
//        repaint();
//    }
    
    public void refresh() {
       repaint();
    }

}
